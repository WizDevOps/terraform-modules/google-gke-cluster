variable project_id {
  type = string
}
variable region {
  type = string
}
variable cluster_name {
  type = string
}
variable monitoring_service {
  default = "none"
}
variable logging_service {
  default = "none"
}
variable network_policy {
  type = bool
}
variable network_policy_provider {
  default = "CALICO"
}
variable machine_type {
  type = string
}
variable preemptible_node_pool {
  type = bool
}
variable node_metadata {
  description = "How to expose the node metadata to the workload running on the node."
  default     = "GKE_METADATA_SERVER"
}
variable node_config_disk_type {
  description = <<EOF
  Available values are:
    pd-standard
    pd-ssd
  Find more about that on the official documentation page
  https://www.terraform.io/docs/providers/google/r/container_cluster.html#disk_type
EOF
  default     = "pd-standard"
}
variable labels {
  type = map(string)
}
variable tags {
  type = list(string)
}
variable oauth_scopes {
  type = list(string)
}
variable istio_disabled {
  default = true
}
variable istio_auth {
  default = "AUTH_NONE"
}
variable maintenance_start_time {
  default = "01:00"
}
variable network {
  description = "A reference (self link) to the VPC network to host the cluster in"
  type        = string
}
variable subnetwork {
  description = "A reference (self link) to the subnetwork to host the cluster in"
  type        = string
}
variable cluster_secondary_range_name {
  description = "The name of the secondary range within the subnetwork for the cluster to use"
  type        = string
}
variable disable_public_endpoint {
  description = "Control whether the master's internal IP address is used as the cluster endpoint. If set to 'true', the master can only be accessed from internal IP addresses."
  type        = bool
  default     = false
}
variable enable_private_nodes {
  description = "Control whether nodes have internal IP addresses only. If enabled, all nodes are given only RFC 1918 private addresses and communicate with the master via private networking."
  type        = bool
  default     = false
}
variable master_ipv4_cidr_block {
  description = "The IP range in CIDR notation to use for the hosted master network. This range will be used for assigning internal IP addresses to the master or set of masters, as well as the ILB VIP. This range must not overlap with any other ranges in use within the cluster's network."
  type        = string
  default     = ""
}
variable master_authorized_networks_config {
  description = <<EOF
  The desired configuration options for master authorized networks. Omit the nested cidr_blocks attribute to disallow external access (except the cluster node IPs, which GKE automatically whitelists)
  ### example format ###
  master_authorized_networks_config = [{
    cidr_blocks = [{
      cidr_block   = "10.0.0.0/8"
      display_name = "example_network"
    }],
  }]

EOF
  type        = list(any)
  default     = []
}

variable nodepool_nodes_per_zone {
  type    = number
  default = 1
}
variable nodepool_autoscaling_nodes_count {
  default = {
    min = 5
    max = 20
  }
}
